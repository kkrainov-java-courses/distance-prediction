package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistancePredictionServiceTest {
    @Test
    void shouldPredictGreaterThanReserve() {
        // Arrange
        DistancePredictionService service = new DistancePredictionService();
        double consumption = 7.9;
        int volume = 20;
        int expected = 248;

        // Act
        int actual = service.predict(consumption, volume);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void shouldPredictLessThanReserve() {
        // Arrange
        DistancePredictionService service = new DistancePredictionService();
        double consumption = 25.4;
        int volume = 1;
        int expected = 0;

        // Act
        int actual = service.predict(consumption, volume);

        // Assert
        assertEquals(expected, actual);
    }
}
